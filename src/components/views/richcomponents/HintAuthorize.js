"use strict";

import React from "react";
import PropTypes from "prop-types";
import MatrixClientPeg from "../../../MatrixClientPeg";
import * as HtmlUtils from "../../../HtmlUtils";
import axios from "axios";
import ProgressButton from "react-progress-button";
import { _t } from "../../../languageHandler";
import { extract } from "../../../tekos/utils/_ApiUtils";

export default class HintAuthorize extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        hint: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.state = {
            buttonState: "",
            error: false,
            success: false,
            email: null,
            senderId: ""
        };
        this.onClick = this.onClick.bind(this);
    }
    componentWillMount() {
        if (!this.props.hint.url) {
            this.setState({ buttonState: "disabled" });
        }
        const client = MatrixClientPeg.get();
        this.setState({ senderId: client.getUserId() });
        client.getThreePids().then(addresses => {
            let emails = addresses.threepids.filter(a => a.medium === "email");

            if (emails.length > 0) {
                this.setState({ email: emails[0].address });
            }
        });
    }

    onClick() {
        this.setState({ buttonState: "loading", success: false, error: false });

        let posturl = this.props.hint.url;
        var data = {
            email: this.state.email,
            roomId: this.props.mxEvent.getRoomId(),
            senderId: this.state.senderId,
            ...this.props.hint
        };
        var header = { headers: { "Content-Type": "application/json" } };
        if (posturl.indexOf("@") > -1) {
            var user = extract(posturl, "//", ":");
            var pass = extract(posturl, user + ":", "@");
            if (pass.indexOf("%40") > -1) {
                pass = pass.replace("%40", "@");
            }
            header = {
                auth: {
                    username: user,
                    password: pass
                }
            };
        }
        let me = this;
        axios
            .post(posturl, data, header)
            .then(res => {
                me.setState({ buttonState: "success" });
                me.setState({ error: false, success: true });
                /*   setTimeout(() => {
                    me.setState({ buttonState: "disabled" });
                }, 500); */
            })
            .catch(error => {
                console.error(error);
                me.setState({ buttonState: "error" });
                me.setState({ error: true, success: false });
            });
    }

    render() {
        const hint = this.props.hint;
        let body;

        if (hint.formatted_body) {
            body = HtmlUtils.bodyToHtml(hint);
        } else if (hint.body) {
            body = hint.body;
        }

        return (
            <ProgressButton
                onClick={this.onClick}
                state={this.state.buttonState}
            >
                {_t("Authorize email access")}
            </ProgressButton>
        );
    }
}
