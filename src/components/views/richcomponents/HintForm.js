"use strict";

import React from "react";
import { _t } from "../../../languageHandler";
import axios from "axios";
import Select from "react-select";
import Loader from "react-loader-spinner";
import AceEditor from "react-ace";
import { extract } from "../../../tekos/utils/_ApiUtils";
import ReactHtmlParser, {
    processNodes,
    convertNodeToElement,
    htmlparser2
} from "react-html-parser";

import "brace/mode/json";
import "brace/theme/github";

export default class HintForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { value: "", code: props.default };
        this.state = {
            loading: false,
            error: false,
            success: false,
            errortext: _t(
                "Server unavailable, overloaded, or something else went wrong."
            )
        };
        //this.editorVal
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentWillMount() {
        var model = this.props.model;
        model.map((m, index) => {
            if (m.defaultValue) {
                this.setState({
                    [m.key]: m.defaultValue
                });
            }
            if (m.token) {
                this.setState({
                    [m.key]: m.token.defaultValue
                });
            }
        });
    }
    handleChange(event) {
        this.setState({ value: event.target.value });
    }

    handleSubmit(event) {
        this.setState({ loading: true, success: false, error: false });
        let me = this;
        let url = this.props.postUrl;
        var header = { headers: { "Content-Type": "application/json" } };
        if (url.indexOf("@") > -1) {
            var user = extract(url, "//", ":");
            var pass = extract(url, user + ":", "@");
            if (pass.indexOf("%40") > -1) {
                pass = pass.replace("%40", "@");
            }
            header = {
                auth: {
                    username: user,
                    password: pass
                }
            };
            console.warn(header);
        }

        let data = {
            senderId: localStorage.getItem("mx_user_id"),
            roomId: localStorage.getItem("mx_last_room_id"),
            ...this.state
        };

        axios
            .post(url, data, header)
            .then(res => {
                me.setState({ loading: false });
                me.setState({ error: false, success: true });
                console.warn(res);
            })
            .catch(error => {
                console.warn(error);
                console.warn(error.response);
                if (error.response.data) {
                    if (error.response.data.error) {
                        me.setState({ errortext: error.response.data.error });
                    }
                }
                me.setState({ loading: false });
                me.setState({ error: true, success: false });
            });
        event.preventDefault();
    }
    _onChange(value, key) {
        this.setState({ error: false, success: false });
        console.warn(value);
        console.warn(key.value);
        this.setState({
            [value]: key.value
        });
    }
    onSubmit(e) {
        e.preventDefault();
        if (this.props.onSubmit) this.props.onSubmit(this.state);
    }
    onChange(e, key, max) {
        this.setState({ error: false, success: false });
        this.setState({
            [key]: e.target.value
        });
        if (max > 0) {
            let namekey = [key] + "remain";
            this.setState({
                [namekey]: max - e.target.value.length
            });
        }
    }
    codeChange(e, key) {
        this.state[key] = e;
    }
    loadCode(e, key) {}
    renderForm() {
        const colourStyles = {
            control: styles => ({
                ...styles,
                backgroundColor: "white",
                margin: "9px 0px",
                marginBottom: "10px",
                borderRadius: "5px",
                color: "black"
            })
        };

        var inputGreen = {
            padding: "10px 10px",
            width: "100%",
            outline: 0,
            marginBottom: "20px",
            borderRadius: "5px",
            border: 0,
            backgroundColor: "#ededed",
            fontSize: "14px",
            WebkitAppearance: "none",
            MozAppearance: "none",
            appearance: "none",
            margin: "9px 0px",
            color: "black"
        };
        var textAreaGreen = {
            width: "100%",
            padding: "10px 10px 10px 10px",
            outline: 0,
            marginBottom: "20px",
            borderRadius: "5px",
            border: 0,
            backgroundColor: "#ededed",
            fontSize: "14px",
            WebkitAppearance: "none",
            MozAppearance: "none",
            appearance: "none",
            margin: "9px 0px",
            color: "black",
            boxSizing: "border-box"
        };

        let model = this.props.model;
        let self = this;
        let formUI = model.map((m, index) => {
            let key = m.key;
            let type = m.subtype || "text";
            let props = m.props || {};
            let label = m.label;
            let element = m.type;
            let defaultVal = m.default;
            let valueDefault = m.defaultValue || "";
            let disabled = false;
            let req = {};
            if (m.required) {
                req["required"] = "required";
            }

            if (m.token) {
                valueDefault = m.token.defaultValue || "";
                if (m.token.disabled) {
                    disabled = true;
                }
            }

            if (element == "input") {
                if (m.maxLength) {
                    var remain;
                    var dynamickey = [key] + "remain";
                    if (this.state[dynamickey]) {
                        remain = (
                            <p>
                                {_t("Characters Left")}:{" "}
                                {this.state[dynamickey]}
                            </p>
                        );
                    }
                    return (
                        <div key={key}>
                            <label
                                key={m.key + "1"}
                                className="labelGreen"
                                htmlFor={m.key}
                            >
                                {ReactHtmlParser(label)}
                            </label>
                            <input
                                onChange={e => {
                                    this.onChange(e, key, m.maxLength);
                                }}
                                key={"i" + m.key}
                                ref={key => {
                                    this[m.key] = key;
                                }}
                                style={inputGreen}
                                type={type}
                                maxLength={m.maxLength}
                                disabled={disabled}
                                {...req}
                            />
                            {remain}
                        </div>
                    );
                } else {
                    return (
                        <div key={key}>
                            <label
                                key={m.key + "1"}
                                className="labelGreen"
                                htmlFor={m.key}
                            >
                                {ReactHtmlParser(label)}
                            </label>
                            <input
                                onChange={e => {
                                    this.onChange(e, key, -1);
                                }}
                                key={"i" + m.key}
                                ref={key => {
                                    this[m.key] = key;
                                }}
                                style={inputGreen}
                                type={type}
                                defaultValue={valueDefault}
                                disabled={disabled}
                                {...req}
                            />
                        </div>
                    );
                }
            } else if (element == "select") {
                let options = m.options;
                var object = {};
                if (valueDefault) {
                    object = {
                        label: valueDefault,
                        value: valueDefault
                    };
                    return (
                        <div key={key}>
                            <label
                                key={m.key + "1"}
                                className="labelGreen"
                                htmlFor={m.key}
                            >
                                {ReactHtmlParser(label)}
                            </label>
                            <Select
                                key={"i" + m.key}
                                ref={key => {
                                    this[m.key] = key;
                                }}
                                onChange={this._onChange.bind(this, key)}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                isSearchable={true}
                                isMulti={m.multi}
                                name={m.key}
                                options={options}
                                styles={colourStyles}
                                defaultValue={object}
                                placeholder={
                                    <div>{_t("Select your option")}</div>
                                }
                                {...req}
                            />
                        </div>
                    );
                } else {
                    return (
                        <div key={key}>
                            <label
                                key={m.key + "1"}
                                className="labelGreen"
                                htmlFor={m.key}
                            >
                                {ReactHtmlParser(label)}
                            </label>
                            <Select
                                key={"i" + m.key}
                                ref={key => {
                                    this[m.key] = key;
                                }}
                                onChange={this._onChange.bind(this, key)}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                isDisabled={false}
                                isLoading={false}
                                isClearable={true}
                                isRtl={false}
                                isSearchable={true}
                                isMulti={m.multi}
                                name={m.key}
                                options={options}
                                styles={colourStyles}
                                placeholder={
                                    <div>{_t("Select your option")}</div>
                                }
                                {...req}
                            />
                        </div>
                    );
                }
            } else if (element == "editor") {
                return (
                    <div key={key}>
                        <label
                            key={m.key + "1"}
                            className="labelGreen"
                            htmlFor={m.key}
                        >
                            {ReactHtmlParser(label)}
                        </label>
                        <AceEditor
                            {...props}
                            mode="json"
                            ref={key => {
                                this[m.key] = key;
                            }}
                            theme="github"
                            onChange={e => {
                                this.codeChange(e, key);
                            }}
                            value={valueDefault}
                            name="{m.key}"
                            wrapEnabled={true}
                            editorProps={{ $blockScrolling: true }}
                        />
                    </div>
                );
            } else if (element == "textarea") {
                if (m.maxLength) {
                    var remain;
                    var dynamickey = [key] + "remain";
                    if (this.state[dynamickey]) {
                        remain = (
                            <p>
                                {_t("Characters Left")}:{" "}
                                {this.state[dynamickey]}
                            </p>
                        );
                    }
                    return (
                        <div key={key}>
                            <label
                                key={m.key + "1"}
                                className="labelGreen"
                                htmlFor={m.key}
                            ></label>
                            <textarea
                                onChange={e => {
                                    this.onChange(e, key, m.maxLength);
                                }}
                                id={"id" + m.key}
                                key={"i" + m.key}
                                ref={key => {
                                    this[m.key] = key;
                                }}
                                rows="3"
                                style={textAreaGreen}
                                maxLength={m.maxLength}
                                defaultValue={valueDefault}
                            ></textarea>
                            {remain}
                        </div>
                    );
                } else {
                    return (
                        <div key={key}>
                            <label
                                key={m.key + "1"}
                                className="labelGreen"
                                htmlFor={m.key}
                            ></label>
                            <textarea
                                onChange={e => {
                                    this.onChange(e, key, -1);
                                }}
                                id={"id" + m.key}
                                key={"i" + m.key}
                                ref={key => {
                                    this[m.key] = key;
                                }}
                                rows="3"
                                style={textAreaGreen}
                                defaultValue={valueDefault}
                            ></textarea>
                        </div>
                    );
                }
            }
        });
        return formUI;
    }

    render() {
        var errortext = this.state.errortext;
        var successtext = _t("your data has been saved successfully");
        const titleAlign = {
            textAlign: "center",
            color: "black"
        };
        const successtyle = {
            color: "#449D44",
            margin: "10px 0px",
            textAlign: "center"
        };
        const errorstyle = {
            color: "red",
            margin: "10px 0px",
            textAlign: "center"
        };
        let title = this.props.title;
        return (
            <div className="ContainerGreen">
                <h3 style={titleAlign}>{ReactHtmlParser(title)}</h3>
                <form
                    className="formGreen"
                    onSubmit={e => {
                        this.handleSubmit(e);
                    }}
                >
                    <div className="large-textareaGreen">
                        {this.renderForm()}
                    </div>
                    <button
                        disabled={this.state.loading}
                        id="submit"
                        className="btnGreen"
                        type="submit"
                        name="submit"
                    >
                        {this.state.loading && (
                            <Loader
                                type="TailSpin"
                                color="#000000"
                                height="25"
                                width="25"
                            />
                        )}
                        {!this.state.loading && this.props.submitTitle}
                    </button>
                    {this.state.error && (
                        <h5 style={errorstyle}> {errortext} </h5>
                    )}
                    {this.state.success && (
                        <h5 style={successtyle}> {successtext} </h5>
                    )}
                </form>
            </div>
        );
    }
}
