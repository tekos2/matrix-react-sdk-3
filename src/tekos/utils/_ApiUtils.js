export function extract(str, prefix, suffix) {
    var s = str;
    var i = s.indexOf(prefix);
    if (i >= 0) {
        s = s.substring(i + prefix.length);
    } else {
        return "";
    }
    if (suffix) {
        i = s.indexOf(suffix);
        if (i >= 0) {
            s = s.substring(0, i);
        } else {
            return "";
        }
    }
    return s;
}
