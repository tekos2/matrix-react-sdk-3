"use strict";

import React from "react";
import PropTypes from "prop-types";
import axios from "axios";

var myWidget;
var allResponse = [];
export default class HintCloud extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        hint: PropTypes.any
    };
    tableToJson(table) {
        var data = []; // first row needs to be headers var headers = [];
        for (var i = 0; i < table.rows[0].cells.length; i++) {
            headers[i] = table.rows[0].cells[i].innerHTML
                .toLowerCase()
                .replace(/ /gi, "");
        }
        // go through cells
        for (var i = 1; i < table.rows.length; i++) {
            var tableRow = table.rows[i];
            var rowData = {};
            for (var j = 0; j < tableRow.cells.length; j++) {
                rowData[headers[j]] = tableRow.cells[j].innerHTML;
            }
            data.push(rowData);
        }
        return data;
    }

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.state = { uploadSuccess: false, uploadDone: false };
    }

    onClick() {
        myWidget.open();
    }
    componentDidMount() {
        const cloud_prop = this.props.hint;
        var params = {
            cloudName: cloud_prop.cloudname,
            uploadPreset: cloud_prop.uploadpreset
        };
        if (cloud_prop.params) {
            if (cloud_prop.params.maxFiles) {
                params.maxFiles = Number(cloud_prop.params.maxFiles);
            }
        }

        myWidget = cloudinary.createUploadWidget(params, (error, result) => {
            console.warn(params);
            if (!error && result && result.event === "success") {
                allResponse.push(result.info);
                this.setState({ uploadSuccess: true });
            }
            if (error && result.event != "success") {
                this.setState({ uploadSuccess: false });
            }
            if (this.state.uploadSuccess && result.event == "close") {
                var data = JSON.parse(JSON.stringify(allResponse));
                var i;
                for (i = 0; i < data.length; i++) {
                    data[i]["room_id"] = this.props.room;
                    data[i]["sender_id"] = this.props.user;
                }

                if (cloud_prop.posturl) {
                    axios
                        .post(cloud_prop.posturl, data)
                        .then(res => {
                            // console.warn(
                            //     "success cloudinary payload to flow"
                            // );
                            // console.warn(res);
                            allResponse = [];
                        })
                        .catch(error => {
                            console.error(error);
                            allResponse = [];
                        });
                }
                this.setState({ uploadSuccess: false });
            }
        });
    }
    render() {
        const button_text = this.props.hint;
        let text_button = "Upload files";
        if (button_text.buttontext) {
            text_button = button_text.buttontext;
        }
        return (
            <div className="pb-container ">
                <button
                    className="pb-button pb-lpm"
                    id="upload_widget"
                    role="link"
                    type="submit"
                    onClick={() => {
                        this.onClick();
                    }}
                >
                    <span>{text_button}</span>
                </button>
            </div>
        );
    }
}
