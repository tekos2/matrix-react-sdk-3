export function TekosOnlyPlateform() {
    return true;
}
export function RedirectToFlowBotWithMessage(name, message) {
    var host = window.location.hostname;
    var protocol = window.location.protocol;
    var port = window.location.port;

    var url;
    if (message != null) {
        url = protocol + "//" + host + "/?msg=" + message + "#/user/" + name;
    } else {
        url = protocol + "//" + host + "/#/user/" + name;
    }

    return url;
}

export function isTherePricingTableParams() {
    const urlParams = new URLSearchParams(window.location.search);
    const isTherePricing = urlParams.get("upgrade");
    let isit = false;
    if (isTherePricing) {
        isit = true;
    }
    /*  if (isIos || isAndroid) {
        isit = true;
    } */
    return isit;
}

export function isThereFirstPromoterParams() {
    const urlParams = new URLSearchParams(window.location.search);
    //  const first_promoter = urlParams.get("_by");
    let isit = false;
    if (urlParams.get("_by")) {
        isit = urlParams.get("_by");
    } else if (urlParams.get("fpr")) {
        isit = urlParams.get("_by");
    } else if (urlParams.get("fp_ref")) {
        isit = urlParams.get("fp_ref");
    } else if (urlParams.get("via")) {
        isit = urlParams.get("via");
    } else if (urlParams.get("deal")) {
        isit = urlParams.get("deal");
    } else if (urlParams.get("_form")) {
        isit = urlParams.get("_form");
    } else if (urlParams.get("_get")) {
        isit = urlParams.get("_get");
    } else if (urlParams.get("_go")) {
        isit = urlParams.get("_go");
    } else if (urlParams.get("_r_")) {
        isit = urlParams.get("_r_");
    }

    return isit;
}

export function isThereTextMessageParams() {
    const urlParams = new URLSearchParams(window.location.search);
    const msg = urlParams.get("msg");
    let isit = false;
    if (msg) {
        return msg;
    }
    /*  if (isIos || isAndroid) {
        isit = true;
    } */
    return isit;
}

export function isThereABubbleLayout() {
    /*  const isIos =
        /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    const isAndroid = /Android/.test(navigator.userAgent); */

    const urlParams = new URLSearchParams(window.location.search);
    const isThereBubble = urlParams.get("bl");
    let isit = false;
    if (isThereBubble) {
        isit = true;
    }
    /*  if (isIos || isAndroid) {
        isit = true;
    } */
    return isit;
}
export function isThereAnEmailAccount() {
    const urlParams = new URLSearchParams(window.location.search);
    const isThereBubble = urlParams.get("email");
    let isit = "";
    if (isThereBubble) {
        isit = isThereBubble;
    }
    return isit;
}
export function isUserLandedOnSlashHome() {
    let isit = false;

    if (window.location.href.indexOf("home") > -1) {
        isit = true;
    }
    return isit;
}
export function isThereWidgetLayout(conf) {
    const urlParams = new URLSearchParams(window.location.search);
    const isThereWidget = urlParams.get("op");
    let isit = false;
    if (isThereWidget) {
        isit = true;
    }
    if (conf == "BOT") {
        isit = true;
    }

    return isit;
}
export function isMobileAndroidOrIos() {
    const isIos =
        /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    const isAndroid = /Android/.test(navigator.userAgent);

    let isit = false;
    if (isIos || isAndroid) {
        isit = true;
    }

    return isit;
}
export function handleLoad() {
    // console.warn("fire select last stamp");
    let allNodes = document.querySelectorAll(".mx_EventTile:last-child");
    var len = allNodes.length;

    let lastNode;
    // console.warn("length table");
    // console.warn(len);
    if (len > 1) {
        lastNode = allNodes[len - 1];
        lastNode.click();
    }
}
export function isThereComposerHide() {
    const urlParams = new URLSearchParams(window.location.search);
    const isThereComposer = urlParams.get("cm");
    let isit = false;
    if (isThereComposer == 0) {
        isit = true;
    }
    return isit;
}
export function insertParam(key, value) {
    key = encodeURI(key);
    value = encodeURI(value);

    var kvp = document.location.search.substr(1).split("&");

    var i = kvp.length;
    var x;
    while (i--) {
        x = kvp[i].split("=");

        if (x[0] == key) {
            x[1] = value;
            kvp[i] = x.join("=");
            break;
        }
    }

    if (i < 0) {
        kvp[kvp.length] = [key, value].join("=");
    }

    //this will reload the page, it's likely better to store this until finished
    document.location.search = kvp.join("&");
}

export const hideLeftPanel = () =>
    Boolean(new URLSearchParams(window.location.search).get("lp"));
