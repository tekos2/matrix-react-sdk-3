"use strict";

import React from "react";
import PropTypes from "prop-types";
import MatrixClientPeg from "../../../MatrixClientPeg";
import * as HtmlUtils from "../../../HtmlUtils";
import axios from "axios";
import Loader from "react-loader-spinner";
import ReactHtmlParser, {
    processNodes,
    convertNodeToElement,
    htmlparser2
} from "react-html-parser";
import { extract } from "../../../tekos/utils/_ApiUtils";

export default class HintCard extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        card: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
        this.state = {
            height: 0,
            maxheight: 0,
            client: null,
            error: false,
            success: false
        };
    }

    onClick(msg) {
        const client = MatrixClientPeg.get();
        let msgType;
        msgType = "m.text";
        client.sendMessage(this.props.mxEvent.getRoomId(), {
            body: msg,
            msgtype: msgType
        });
    }
    refCallback = element => {
        if (element) {
            this.props.getSize(element.getBoundingClientRect());
        }
    };
    validURL(str) {
        var pattern = new RegExp(
            "^(https?:\\/\\/)?" + // protocol
            "((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|" + // domain name
            "((\\d{1,3}\\.){3}\\d{1,3}))" + // OR ip (v4) address
            "(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*" + // port and path
            "(\\?[;&a-z\\d%_.~+=-]*)?" + // query string
                "(\\#[-a-z\\d_]*)?$",
            "i"
        ); // fragment locator
        return !!pattern.test(str);
    }
    OpenUrl(url) {
        window.open(url, "_blank");
    }
    renderJSXElement(v) {
        return JSX.Element(v);
    }
    componentDidMount() {
        const height = this.divElement.clientHeight;
        this.setState({ height });
        const client = MatrixClientPeg.get();
        this.setState({ client: client });
    }
    handleClick = (val, buttontype, obj, index) => {
        // console.log(buttontype);
        console.log("begin handle click");
        console.log(val);
        console.log(obj);
        const client = this.state.client;

        if (buttontype == "url") {
            this.OpenUrl(val);
        }
        if (buttontype == "postback") {
            client.sendMessage(this.props.mxEvent.getRoomId(), {
                body: val,
                msgtype: "m.text"
            });
        }
        if (obj.value && obj.type == "request_url_only") {
            this.setState({ success: false, error: false });
            this.setState({ ["loading" + index]: true });
            let posturl = obj.value;

            var data = {
                id: obj.label,
                msg: obj.value,
                senderid: client.getUserId(),
                roomId: this.props.mxEvent.getRoomId()
            };
            var header = { headers: { "Content-Type": "application/json" } };

            if (posturl.indexOf("@") > -1) {
                var user = extract(posturl, "//", ":");
                var pass = extract(posturl, user + ":", "@");
                if (pass.indexOf("%40") > -1) {
                    pass = pass.replace("%40", "@");
                }
                header = {
                    auth: {
                        username: user,
                        password: pass
                    }
                };
            }

            let me = this;
            axios
                .post(posturl, data, header)
                .then(res => {
                    console.warn("res", res);
                    this.setState({
                        ["loading" + index]: false
                    });
                    me.setState({ error: false, success: true });
                })
                .catch(error => {
                    console.error(error);

                    this.setState({
                        ["loading" + index]: false
                    });
                    me.setState({ error: true, success: false });
                });
        }
    };
    render() {
        const card = this.props.card;
        let body;
        let img;
        let url;
        let title;
        const items = [];
        let bodyandtitle;
        let blockButtons;
        if (card.buttons) {
            for (const [index, butt] of card.buttons.entries()) {
                if (butt.type == "url") {
                    items.push(
                        <button
                            onClick={() =>
                                this.handleClick(butt.url, butt.type, butt)
                            }
                            key={index}
                            className="css-button"
                        >
                            {butt.label}
                        </button>
                    );
                }
                if (butt.type == "postback") {
                    items.push(
                        <button
                            onClick={() =>
                                this.handleClick(butt.value, butt.type, butt)
                            }
                            key={index}
                            className="css-button"
                        >
                            {butt.label}
                        </button>
                    );
                }
                if (butt.type == "request_url_only") {
                    items.push(
                        <button
                            disabled={this.state["loading" + index]}
                            onClick={() =>
                                this.handleClick(
                                    butt.value,
                                    butt.type,
                                    butt,
                                    index
                                )
                            }
                            key={index}
                            className="css-button"
                        >
                            {this.state["loading" + index] && (
                                <Loader
                                    type="TailSpin"
                                    color="#000000"
                                    height="25"
                                    width="25"
                                />
                            )}
                            {!this.state["loading" + index] && butt.label}
                        </button>
                    );
                }
                blockButtons = (
                    <div className="css-wrapper-button">{items}</div>
                );
            }
        }
        if (card.title) {
            title = card.title;
        }
        if (card.formatted_body) {
            body = HtmlUtils.bodyToHtml(card);
        } else if (card.body) {
            body = card.body;
        }
        if (card.title && card.body) {
            bodyandtitle = (
                <div className="css-card-text">
                    <p className="css-title">{ReactHtmlParser(body)}</p>
                    <p className="css-subtitle">{ReactHtmlParser(title)}</p>
                </div>
            );
        }
        if (card.img) {
            if (card.img.startsWith("mxc://")) {
                url = this.state.client.mxcUrlToHttp(card.img);
                if (card.picture_link && this.validURL(card.picture_link)) {
                    img = (
                        <img
                            alt="Card image cap"
                            onClick={this.OpenUrl(card.picture_link)}
                            src={url}
                        />
                    );
                } else {
                    img = <img alt="Card image cap" src={url} />;
                }
            } else if (
                card.img.startsWith("data:") ||
                card.img.startsWith("http://") ||
                card.img.startsWith("https://")
            ) {
                url = card.img;
                if (card.picture_link && this.validURL(card.picture_link)) {
                    img = (
                        <img
                            alt="Card image cap"
                            onClick={this.OpenUrl(card.picture_link)}
                            src={url}
                        />
                    );
                } else {
                    img = <img alt="Card image cap" src={url} />;
                }
            }
        }

        return (
            <div
                className="css-header "
                ref={divElement => (this.divElement = divElement)}
                id={this.props.id + "cardid"}
            >
                <div className="css-img ">{img}</div>
                {bodyandtitle}
                {blockButtons}
            </div>
        );
    }
}
