"use strict";

import React from "react";
import PropTypes from "prop-types";
import MatrixClientPeg from "../../../MatrixClientPeg";
import * as HtmlUtils from "../../../HtmlUtils";
import ModalImage from "react-modal-image";
let stock = "../../../../res/img/image-not-found.jpg";

export default class HintImage extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        hint: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {}

    render() {
        const round = {
            borderRadius: "25px !important"
        };
        const client = MatrixClientPeg.get();
        const hint = this.props.hint;
        let url;
        let largeimg;
        let tinyimage;
        let alt;
        alt = hint.alt;

        if (hint.smallimage) {
            if (hint.smallimage.startsWith("mxc://")) {
                url = client.mxcUrlToHttp(hint.smallimage);
                tinyimage = url;
            } else if (
                hint.smallimage.startsWith("data:") ||
                hint.smallimage.startsWith("http://") ||
                hint.smallimage.startsWith("https://")
            ) {
                tinyimage = hint.smallimage;
            }
        }
        if (hint.bigimage) {
            if (hint.bigimage.startsWith("mxc://")) {
                url = client.mxcUrlToHttp(hint.bigimage);
                largeimg = url;
            } else if (
                hint.bigimage.startsWith("data:") ||
                hint.bigimage.startsWith("http://") ||
                hint.bigimage.startsWith("https://")
            ) {
                largeimg = hint.bigimage;
            }
        }

        return (
            <ModalImage
                style={round}
                small={tinyimage}
                large={largeimg}
                alt={alt}
                className="HintImage_Round"
            />
        );
    }
}
