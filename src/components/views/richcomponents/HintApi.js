"use strict";

import React from "react";
import PropTypes from "prop-types";
import { _t } from "../../../languageHandler";

export default class HintApi extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        hint: PropTypes.any
    };
    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        const hint = this.props.hint;
        const session_id = hint.sessionid;

        const stripe = Stripe(hint.token_stripe);

        stripe
            .redirectToCheckout({
                // Make the id field from the Checkout Session creation API response
                // available to this file, so you can provide it as parameter here
                // instead of the {{CHECKOUT_SESSION_ID}} placeholder.
                sessionId: session_id
            })
            .then(result => {
                // console.warn("##### STRIPE ######");
                // console.warn(result);
                // If `redirectToCheckout` fails due to a browser or network
                // error, display the localized error message to your customer
                // using `result.error.message`.
            });
    }

    render() {
        const buttontext = this.props.hint.buttontext;
        var checkout = "Checkout";
        if (buttontext) {
            checkout = buttontext;
        }

        if (
            this.props.hint.sessionid == null ||
            this.props.hint.token_stripe == null
        ) {
            return (
                <button
                    className="HintApi_button_error"
                    id="checkout-button-cms_month_plan_error"
                    role="link"
                >
                    {_t("Stripe missing")}
                </button>
            );
        } else {
            return (
                <button
                    className="HintApi_button"
                    id="checkout-button-cms_month_plan"
                    role="link"
                    type="submit"
                    onClick={() => {
                        this.onClick();
                    }}
                >
                    {checkout}
                </button>
            );
        }
    }
}
