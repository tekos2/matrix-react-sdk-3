"use strict";

import React from "react";
import PropTypes from "prop-types";
import MatrixClientPeg from "../../../MatrixClientPeg";
import * as HtmlUtils from "../../../HtmlUtils";
import axios from "axios";
import { extract } from "../../../tekos/utils/_ApiUtils";

export default class HintButton extends React.Component {
    static propTypes = {
        mxEvent: PropTypes.object.isRequired, // event with hints
        hint: PropTypes.any
    };

    constructor(props) {
        super(props);
        this.onClick = this.onClick.bind(this);
    }

    onClick() {
        let txtToSend, msgType;
        const client = MatrixClientPeg.get();

        txtToSend = this.props.hint.reply
            ? this.props.hint.reply
            : this.props.hint.body;
        msgType = this.props.hint.replynotify ? "m.notice" : "m.text";
        if (this.props.hint.type == "postback") {
            client.sendMessage(this.props.mxEvent.getRoomId(), {
                body: txtToSend,
                msgtype: msgType
            });
        }

        if (
            this.props.hint.request_url ||
            this.props.hint.type == "request_url_only"
        ) {
            let posturl = this.props.hint.request_url;
            var header = { headers: { "Content-Type": "application/json" } };
            if (posturl.indexOf("@") > -1) {
                var user = extract(posturl, "//", ":");
                var pass = extract(posturl, user + ":", "@");
                if (pass.indexOf("%40") > -1) {
                    pass = pass.replace("%40", "@");
                }
                header = {
                    auth: {
                        username: user,
                        password: pass
                    }
                };
            }
            var data = {
                id: this.props.hint.body,
                msg: txtToSend,
                senderid: client.getUserId(),
                roomid: this.props.mxEvent.getRoomId()
            };
            axios
                .post(posturl, data, header)
                .then(res => {})
                .catch(error => {
                    console.error(error);
                });
        }
    }

    render() {
        var astyle = {
            textDecoration: "none",
            color: "#027be3"
        };
        const client = MatrixClientPeg.get();
        const hint = this.props.hint;
        let body;
        let img;
        let url;
        let buttonurl;

        if (hint.formatted_body) {
            body = HtmlUtils.bodyToHtml(hint);
        } else if (hint.body) {
            body = hint.body;
        }
        if (hint.img) {
            if (hint.img.startsWith("mxc://")) {
                url = client.mxcUrlToHttp(hint.img);
                img = <img src={url} />;
            } else if (hint.img.startsWith("data:")) {
                img = <img src={hint.img} />;
            }
        }

        if (hint.type == "url") {
            buttonurl = hint.value;

            return (
                <div className="mx_HintButton">
                    {" "}
                    <a
                        style={astyle}
                        href={buttonurl}
                        target="_blank"
                        href={buttonurl}
                    >
                        {img}
                        {body}
                    </a>
                </div>
            );
        } else {
            return (
                <div
                    className="mx_HintButton"
                    style={astyle}
                    onClick={this.onClick}
                >
                    {img}
                    {body}
                </div>
            );
        }
    }
}
